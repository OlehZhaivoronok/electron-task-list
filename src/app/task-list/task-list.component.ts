import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TaskListService } from './task-list.service';
import { Task } from './task/task';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskListComponent {

  public newTask = new Task();
  public constructor(private readonly taskListService: TaskListService) { }

  public addTask() {
    this.taskListService.addTask(this.newTask);
    this.newTask = new Task();
  }

  public removeTask(task: Task) {
    if (task.id) this.taskListService.deleteTaskById(task.id);
  }
  
  public completedTask(task: Task) {
    if (task.id) this.taskListService.updateTask(task);
  }

  public get taskList() {
    return this.taskListService.getTaskList();
  }
}
