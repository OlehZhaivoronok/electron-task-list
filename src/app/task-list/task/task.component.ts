import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Task } from './task';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskComponent implements OnInit {

  @Input() public task!: Task;
  @Output() public removeTask: EventEmitter<Task>  = new EventEmitter();
  @Output() public completedTask: EventEmitter<Task>  = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  public remove() {
    this.removeTask.emit(this.task);
  }

  public completed() {
    this.task.completed = !this.task.completed;
    this.completedTask.emit(this.task);
  }

}
