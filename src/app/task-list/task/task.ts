export type TaskStatus ='new' | 'in progress' | 'finished'
export class Task {
  public id: string | undefined;
  public title = '';
  public description = '';
  public status: TaskStatus = 'new';
  public completed = false;

  constructor(values: Partial<Task> = {}) {
    Object.assign(this, values);
  }
}
