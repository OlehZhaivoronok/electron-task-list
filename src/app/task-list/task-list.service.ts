import { Injectable } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';
import { FileService } from '../shared/file.service';
import { Task } from './task/task';

@Injectable()
export class TaskListService {
  private taskList: Task[] = [];

  constructor(private readonly fileService: FileService) {
    this.taskList = this.fileService.getFilesWithData('TaskList', 'Task');
  }

  public addTask(task: Task): TaskListService {
    if (!task.id) {
      task.id = uuidv4();
    }
    this.fileService.saveFile('TaskList', 'Task', task.id, JSON.stringify(task));
    this.taskList.push(task);
    return this;
  }

  public deleteTaskById(id: string): TaskListService {
    this.fileService.deleteFile('TaskList', 'Task', id);
    this.taskList = this.taskList.filter((task) => task.id !== id);
    return this;
  }
  
  public updateTask(task: Task): Task {
    if (!task.id) return task;
    const {id, ...values} = task
    this.fileService.saveFile('TaskList', 'Task', id, JSON.stringify(values));
    Object.assign(task, values);
    return task;
  }

  public getTaskList(): Task[] {
    return this.taskList;
  }

  public getTaskById(id: string): Task {
    return this.taskList.filter((task) => task.id === id)[0];
  }
}
