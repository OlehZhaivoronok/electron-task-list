import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskListService } from './task-list/task-list.service';
import { FileService } from './shared/file.service';
import { TaskComponent } from './task-list/task/task.component';
import { AppMaterialModule } from './material.module';

@NgModule({
  declarations: [AppComponent, TaskListComponent, TaskComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppMaterialModule,
  ],
  exports: [AppMaterialModule],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'fill' },
    },
    TaskListService,
    FileService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
