import { Injectable } from '@angular/core';
import fs from 'fs';

interface ElectronFs {
  readFileSync: typeof fs.readFileSync;
  mkdirSync: typeof fs.mkdirSync;
  readdirSync: typeof fs.readdirSync;
  statSync: typeof fs.statSync;
  writeFileSync: typeof fs.writeFileSync;
  unlinkSync: typeof fs.unlinkSync;
}

interface ElectronWindow {
  require: (name: string) => ElectronFs;
  __dirname: string;
}

type dbs = 'TaskList';
type tables = 'Task';

@Injectable()
export class FileService {
  private fs!: ElectronFs;
  private readonly dirName!: string;

  public constructor() {
    if ((<any>window).require) {
      try {
        this.fs = (<ElectronWindow>window).require('fs');
        this.dirName = (<ElectronWindow>window).__dirname + '/../';
      } catch (error) {
        throw error;
      }
    } else {
      console.warn('Could not load electron');
    }
  }

  private createFolderIfNotExist(db: dbs, table: tables) {
    const dirPath = this.getDir(db, table);
    this.fs.mkdirSync(dirPath, { recursive: true });
  }

  private getDir(db: dbs, table: tables) {
    return `${this.dirName}/${db}/${table}`;
  }

  private getFiles(db: dbs, table: tables) {
    const dir = this.getDir(db, table);
    this.createFolderIfNotExist(db, table);
    const files = this.fs.readdirSync(`${dir}`);
    return files.sort((a: string, b: string) => {
      return (
        this.fs.statSync(dir + '/' + a).mtime.getTime() -
        this.fs.statSync(dir + '/' + b).mtime.getTime()
      );
    });
  }

  public saveFile(db: dbs, table: tables, id: string, data: string): boolean {
    this.createFolderIfNotExist(db, table);
    this.fs.writeFileSync(`${this.getDir(db, table)}/${id}`, data, {
      encoding: 'utf-8',
    });
    return true;
  }

  public deleteFile(db: dbs, table: tables, id: string): boolean {
    this.createFolderIfNotExist(db, table);
    this.fs.unlinkSync(`${this.getDir(db, table)}/${id}`);
    return true;
  }

  public getFilesWithData(db: dbs, table: tables) {
    const files = this.getFiles(db, table);
    return files.map((file: string) => {
      const jsonData = JSON.parse(
        this.fs.readFileSync(`${this.getDir(db, table)}/${file}`, {
          encoding: 'utf-8',
        }) as string
      );
      return { id: file, ...jsonData };
    });
  }
}
